��    1      �  C   ,      8  
   9  
   D     O  	   R  +   \     �     �  
   �     �     �     �  8   �            _   $  W   �     �  	   �     �  
             .     <  %   K     q     y     �  F   �     �     �     �  \        i     q  T   z  !   �     �            (     I     i  #   �      �     �      �  "   	      2	     S	  8  q	     �
  
   �
     �
     �
  Z   �
     >     K     \     k  ,   �  "   �  O   �     "     /  �   >  }        �     �     �     �     �  "   �  !     5   %     [     o     �  w   �  0        H      `  �   �     :  
   I  �   T     �     �            
        #     ,     ;     P     c     p  
        �     %                 $   '   	                                                ,             1   .              &             +   0   -             !   *                  #   
   "   )   (   /                                                 % Comments %1$s: %2$s ,  1 Comment Add widgets here to appear in your sidebar. Archives Archives: %s Author: %s Category: %s Comment navigation Comments are closed. Continue reading %s <span class="meta-nav">&rarr;</span> Day: %s Edit It looks like nothing was found at this location. Maybe try one of the links below or a search? It seems we can&rsquo;t find what you&rsquo;re looking for. Perhaps searching can help. Leave a comment Month: %s Montserrat font: on or offon Navigation Newer Comments Nothing Found Older Comments Oops! That page can&rsquo;t be found. Page %s Pages: Proudly powered by %s Ready to publish your first post? <a href="%1$s">Get started here</a>. Search Results for: %s Sidebar Skip to content Sorry, but nothing matched your search terms. Please try again with some different keywords. Tag: %s Year: %s comments titleOne thought on &ldquo;%2$s&rdquo; %1$s thoughts on &ldquo;%2$s&rdquo; daily archives date formatF j, Y http://wordpress.org/ monthly archives date formatF Y post format archive titleAsides post format archive titleAudio post format archive titleChats post format archive titleGalleries post format archive titleImages post format archive titleLinks post format archive titleQuotes post format archive titleStatuses post format archive titleVideos yearly archives date formatY PO-Revision-Date: 2016-03-08 23:49:25+0000
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=3; plural=(n%10==1 && n%100!=11 ? 0 : n%10>=2 && n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2);
X-Generator: GlotPress/1.1.0-alpha
Project-Id-Version: Magnus
 Коментарів: % %1$s: %2$s ,  1 Коментар Додайте сюди віджети для показу в бічній колонці. Архіви Архіви: %s Автор: %s Категорія: %s Навігація по коментарях Коментарі закриті. Продовжувати читання %s <span class="meta-nav">&rarr;</span> День: %s Змінити Нічого не знайдено. Спробуйте використати один з наведених нижче посилань, або скористайтесь форму пошуку. Схоже, що не вдалося знайти те, що ви шукаєте. Можливо допоможе пошук. Коментувати Місяць: %s off Навігація Новіші коментарі Нічого не Знайдено Старіші коментарі Отакої! Сторінка не знайдена. Сторінка %s Сторінки: Сайт працює на %s Готові опублікувати свій перший запис? <a href="%1$s">Почніть звідси</a>. Пошукові результати для: %s Бічна панель Перейти до вмісту Вибачте, але за вашим пошуковим запитом нічого не знайдено. Будь ласка, спробуйте інші ключові слова. Мітки: %s Рік: %s %1$s коментар до &ldquo;%2$s&rdquo; %1$s коментарі до &ldquo;%2$s&rdquo; %1$s коментарів до &ldquo;%2$s&rdquo; d.m.Y http://uk.wordpress.org/ F Y Примітки Аудіо Чати Галереї Зображення Посилання Цитати Статуси Відео Y 