<?php

get_header(); ?>
    		<?php $bloginfo = str_replace(' ', '<br>', get_bloginfo()); 
                    if(get_bloginfo('language')=='ru-RU' ){
                        $lastsob='Последние события';
                        $readmore='Читать далее';
                        $month_array = array("01"=>"Январь","02"=>"Февраль","03"=>"Март","04"=>"Апрель","05"=>"Май", "06"=>"Июнь", "07"=>"Июль","08"=>"Август","09"=>"Сентябрь","10"=>"Октябрь","11"=>"Ноябрь","12"=>"Декабрь");
                    }
                    if(get_bloginfo('language')=='en-US' ){
                        $lastsob='Latest events';
                        $readmore='Read more';
                        $month_array = array("01"=>"January","02"=>"February","03"=>"March","04"=>"April","05"=>"May", "06"=>"June", "07"=>"July","08"=>"August","09"=>"September","10"=>"October","11"=>"November","12"=>"December");
                    }
?> 			
		<div class="main clearfix">

			<div class="container">

				<div class="main__top">
					<h1 class="main__title"><?php echo $bloginfo; ?></h1>
					<span class="main__prof"><?php  echo bloginfo( 'description' ); ?></span>
				</div>

			</div>

			<div class="main__calendare">
				<div class="container">
<span class="calendare__title"><?php echo $lastsob; ?></span>
                       <ul class="calendare__list clearfix">
                        <?php 
        $temp = $wp_query; $wp_query= null;
        $curldate=date( 'm', current_time( 'timestamp' ))-3;
        $mountplus=$curldate;              
        $permount=0;
        $curlpermount=0;
        $setting=0;
        $wp_query = new WP_Query(
                                array( 'category_name' => 'Последние события',
                                       'post_status' => 'publish',
                                       'orderby' => 'date',
                                       'order'   => 'ASC'
                                   ));
        for ($i = 1; $i <= 12; $i++) {
            $permount=0;
            
        while ($wp_query->have_posts()) : $wp_query->the_post(); ?>
               <?php
                    $goldata = the_date('Y-m-d', '', '', FALSE);
                    $thedate = date('m',strtotime($goldata));
                     
                    
if($setting<4){
                    if($thedate==$mountplus){
                       
                        if($permount==0){
                             $setting++;
                            $m = $month_array[date($thedate)]; ?>
                            <li style="float: left;">
                            <div class="data__title"><?php  echo $m; ?></div>
                            <div class="data__count">
                            
                      
                           
                         <?php    $permount=1; }?>
                         
                       <div class="data__num data__num-1">
									<span> <?php echo date('d',strtotime($goldata)); ?> </span>
									<div class="event__block" style='background: url(<?php echo the_post_thumbnail_url(full); ?>) 0 0 no-repeat;'>
										<div class="event__overlay"></div>
										<div class="event__wrap">
											<h4 class="event__title"> <?php the_title(); ?></h4>
											<div class="event__descr">
												<p><?php the_excerpt(); ?></p>
											</div>
											<a href="<? echo get_permalink(); ?>" class="link__more"><?php echo $readmore; ?><i></i></a>
										</div>
									</div>
								</div>
                         <?php
                            
                                                        }
}
                        
                    ?>
              
               
 
        <?php endwhile;  
            if($mountplus==12){$mountplus=0;}
            $mountplus++;
        } ?>
 
 
        <?php wp_reset_postdata(); ?>
          </ul>      
					
				
				</div>

			</div>
		</div>

		<div class="line__right"></div>

<?php get_sidebar(); ?>
<?php get_footer(); ?>


