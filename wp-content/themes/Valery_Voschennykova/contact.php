<?php
/*
Template Name: Контакты
*/

get_header(); ?>
     <?php if(get_bloginfo('language')=='ru-RU' ){
                        $fonen='Телефон';
                        $press='Пресс-кит';
                        $biog='Биография';
                        $ress='Резюме';
                        $obsv='Обратная связь';
                        
                    }
                    if(get_bloginfo('language')=='en-US' ){
                        $fonen='Phone';
                        $press='Press kit';
                        $biog='Biography';
                        $ress='Resume';
                        $obsv='Feedback';
                        
                    } ?>
			<div class="main clearfix">

			<div class="container">

				<div class="main__top main__top-inner main__top-about">
					<?php the_title( '<h1 class="main__title main__title-inner main__title-about">', '</h1>' ); ?></h1>
				</div>

           
			
		
	
				<div class="contact__block">
					<div class="contact__item contact__item-phone"><?php echo $fonen; ?>:
					<a href="tel:<?php echo get_option('site_telephone1'); ?>"><?php echo get_option('site_telephone1'); ?></a>
					</div>

					<div class="contact__item contact__item-mail">E-mail:
					<a href="mailto:<?php echo get_option('site_telephone2'); ?>"><?php echo get_option('site_telephone2'); ?></a>
					</div>

    
				
					<div class="contact__social clearfix">
						<a href="<?php echo get_option('theme_ftext'); ?>" class="fb__link" target="_blank">Facebook</a>
						<a href="<?php echo get_option('theme_ttext'); ?>" class="inst__link" target="_blank">Instagram</a>
						<a href="<?php echo get_option('theme_gtext'); ?>" class="yt__link" target="_blank">YouTube</a>
					</div>

                
					<div class="contact__form">
					<span class="form__title form__title-contact"><?php echo $obsv; ?></span>
					<?php if(get_bloginfo('language')=='ru-RU' ){
                       echo do_shortcode('[contact-form-7 id="194" title="Контактная форма 1"]'); 
                    }
                    if(get_bloginfo('language')=='en-US' ){
                       echo do_shortcode('[contact-form-7 id="195" title="contactform"]'); 
                    } ?>
					
						
						
					</div> 
          	<div id="press-kit" class="press__kit">
						<div class="press__title"><?php echo $press; ?></div>
						<a href="<?php echo get_field('biografi'); ?>" class="press__link press__link-one" target="_blank"><?php echo $biog='Biography'; ?>                       </a>
						<a href="<?php echo get_field('biografi'); ?>" class="press__link press__link-two" target="_blank"><?php echo $ress='Resume'; ?></a>
					</div>
			</div>
			
			</div>

		</div>

	
	
	
	

<?php get_sidebar(); ?>
<?php get_footer(); ?>



