<?php
/**
 * The template for displaying the footer.
 *
 * Contains the closing of the #content div and all content after
 *
 * @package Magnus
 */
?>
    <?php 



                    if(get_bloginfo('language')=='ru-RU' ){
                        $divby='разработано в';
                        $divbyname='Рекламотерапия';
                        $pregTable = '~{:ru}(.*?){:}~is';
                    }
                    if(get_bloginfo('language')=='en-US' ){
                        $divby='designed in';
                        $divbyname='Reklamoterapiya';
                        $pregTable = '~{:en}(.*?){:}~is';
                    }

$file = get_option('site_foto');
preg_match($pregTable, $file, $arrTable);
$fotoautor= end($arrTable);

?> 			

	<!-- #content -->
			<footer class="clearfix">
			<div class="container">
				<div class="copyright">&copy; 2016 | <?php echo get_bloginfo(); ?>.</div>
				<div class="photo__dev"><?php echo $fotoautor ?></div>
				
					<div class="footer__social">
					<a href="<?php echo get_option('theme_ftext'); ?>" class="fb" target="_blank">facebook</a>
					<a href="<?php echo get_option('theme_ttext'); ?>" class="yt" target="_blank">youtube</a>
					<a href="<?php echo get_option('theme_gtext'); ?>" class="inst" target="_blank">instagram</a>
				</div>
						
				<div class="footer__contact">
					<a href="tel:<?php echo get_option('site_telephone1'); ?>" class="contact__phone" title="Позвонить"><?php echo get_option('site_telephone1'); ?></a>
					<a href="mailto:<?php echo get_option('site_telephone2'); ?>" class="contact__mail" title="Написать письмо"><?php echo get_option('site_telephone2'); ?></a>
					<div class="developer"><?php echo $divby; ?> <a href="#"><?php echo $divbyname; ?></a></div>
				</div>
			</div>
		</footer>
</div><!-- #page -->

<?php wp_footer(); ?>
<div class="hidden"></div>

	<div class="loader">
		<div class="loader_inner"></div>
	</div>

	<!--[if lt IE 9]>
	<script src="<?php echo esc_url( get_site_url() ); ?>/wp-content/themes/Valery_Voschennykova/libs/html5shiv/es5-shim.min.js"></script>
	<script src="<?php echo esc_url( get_site_url() ); ?>/wp-content/themes/Valery_Voschennykova/libs/html5shiv/html5shiv.min.js"></script>
	<script src="<?php echo esc_url( get_site_url() ); ?>/wp-content/themes/Valery_Voschennykova/libs/html5shiv/html5shiv-printshiv.min.js"></script>
	<script src="<?php echo esc_url( get_site_url() ); ?>/wp-content/themes/Valery_Voschennykova/libs/respond/respond.min.js"></script>
	<![endif]-->

	<script src="<?php echo esc_url( get_site_url() ); ?>/wp-content/themes/Valery_Voschennykova/libs/jquery/jquery-1.12.4.min.js"></script>
	<script src="<?php echo esc_url( get_site_url() ); ?>/wp-content/themes/Valery_Voschennykova/libs/magnific/jquery.magnific-popup.js"></script>
	<script src="<?php echo esc_url( get_site_url() ); ?>/wp-content/themes/Valery_Voschennykova/libs/mediaelement/mediaelement-and-player.js"></script>

	<script src="<?php echo esc_url( get_site_url() ); ?>/wp-content/themes/Valery_Voschennykova/js/common.js"></script>
</body>
</html>

