<!DOCTYPE html>
<html <?php language_attributes(); ?> class="no-js">
<head>
<meta charset="<?php bloginfo( 'charset' ); ?>">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">
<meta charset="utf-8">

	<title>Вощенникова Валерия</title>
	<meta name="description" content="">

	<meta property="og:image" content="<?php echo esc_url( get_site_url() ); ?>/wp-content/themes/Valery_Voschennykova/img/Voshennikova.jpg">
	<link rel="shortcut icon" href="<?php echo esc_url( get_site_url() ); ?>/wp-content/themes/Valery_Voschennykova/img/favicon/favicon.ico" type="image/x-icon">
	<link rel="apple-touch-icon" href="<?php echo esc_url( get_site_url() ); ?>/wp-content/themes/Valery_Voschennykova/img/favicon/apple-touch-icon.png">
	<link rel="apple-touch-icon" sizes="72x72" href="<?php echo esc_url( get_site_url() ); ?>/wp-content/themes/Valery_Voschennykova/img/favicon/apple-touch-icon-72x72.png">
	<link rel="apple-touch-icon" sizes="114x114" href="<?php echo esc_url( get_site_url() ); ?>/wp-content/themes/Valery_Voschennykova/img/favicon/apple-touch-icon-114x114.png">

	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">

	<link rel="stylesheet" href="<?php echo esc_url( get_site_url() ); ?>/wp-content/themes/Valery_Voschennykova/libs/normalize/normilize.css">
	<link rel="stylesheet" href="<?php echo esc_url( get_site_url() ); ?>/wp-content/themes/Valery_Voschennykova/css/fonts.css">
	<link rel="stylesheet" href="<?php echo esc_url( get_site_url() ); ?>/wp-content/themes/Valery_Voschennykova/css/main.css">
	<link rel="stylesheet" href="<?php echo esc_url( get_site_url() ); ?>/wp-content/themes/Valery_Voschennykova/css/media.css"><link rel="stylesheet" href="<?php echo esc_url( get_site_url() ); ?>/wp-content/themes/Valery_Voschennykova/style.css">

	<link rel="stylesheet" href="<?php echo esc_url( get_site_url() ); ?>/wp-content/themes/Valery_Voschennykova/libs/magnific/magnific-popup.css">
	<link rel="stylesheet" href="<?php echo esc_url( get_site_url() ); ?>/wp-content/themes/Valery_Voschennykova/libs/mediaelement/mediaelementplayer.css">

	<script src="<?php echo esc_url( get_site_url() ); ?>/wp-content/themes/Valery_Voschennykova/libs/modernizr/modernizr.js"></script>


</head>

<body <?php body_class(); ?>>
	<?php $wrapper="wrapper"; if (is_home()){ $wrapper="wrapper";}  ?>
	<?php if ( is_page_template( 'aboutme.php' ) ) { $wrapper="wrapper wrapper__inner wrapper__inner-about";} ?>
	<?php if ( is_page_template( 'repertory.php' ) ) { $wrapper="wrapper wrapper__inner wrapper__inner-about";} ?>
	<?php if ( is_page_template( 'photo.php' ) ) { $wrapper="wrapper wrapper__inner-media";} ?>
	<?php if ( is_page_template( 'audio.php' ) ) { $wrapper="wrapper wrapper__inner-media wrapper__inner-audio";} ?>
	<?php if ( is_page_template( 'video.php' ) ) { $wrapper="wrapper wrapper__inner-media";} ?>
	<?php if ( is_page_template( 'concert.php' ) ) { $wrapper="wrapper wrapper__inner-concert";} ?>
	<?php if ( is_page_template( 'party.php' ) ) { $wrapper="wrapper wrapper__inner-party";} ?>
	<?php if ( is_page_template( 'contact.php' ) ) { $wrapper="wrapper wrapper__inner wrapper__inner-contact";} ?>
	<?php if ( is_page_template( 'presskit.php' ) ) { $wrapper="wrapper wrapper__inner-press";} ?>


<div class="<?php echo $wrapper; ?>">
		<header class="clearfix">
			<div class="container">
				<div class="change__lang">
				<?php 
    if ( class_exists( 'WPGlobus' ) ) {
				$flag = WPGlobus::Config()->flags_url . WPGlobus::Config()->flag[ WPGlobus::Config()->language ];
				echo '<a href="' . WPGlobus_Utils::localize_current_url( WPGlobus::Config()->language ). '">';		
        echo WPGlobus::Config()->language;
        echo '</a>';
        ?>
        <span>/</span>
        <?php
				foreach( WPGlobus::Config()->enabled_languages as $lang ) {

					if ( $lang == WPGlobus::Config()->language ) {
						continue;
					}
					$flag = WPGlobus::Config()->flags_url . WPGlobus::Config()->flag[ $lang ];
					echo '<a href="' . WPGlobus_Utils::localize_current_url( $lang ). '" class="lang__one active">';
					echo $lang ;
					echo '</a>';

				}
			
		}
   // echo get_bloginfo('language'); 
                    ?>
				</div>
				<a href="#" id="menu-btn" class="menu__btn"><b></b><b></b><b></b></a>
			</div>
		</header>


