<?php
/*
Template Name: repertory
*/

get_header(); ?>

<div class="main clearfix">

			<div class="container">

				<div class="main__top main__top-inner main__top-about">
				<?php the_title( '<h1 class="main__title main__title-inner main__title-about">', '</h1>' ); ?></h1>
				</div>

				<div class="repertory__block maxheightset">

					<?php
    the_post();
    the_content(); 
?>
				</div>

			</div>

		</div>        
        

<?php get_sidebar(); ?>
<?php get_footer(); ?>



