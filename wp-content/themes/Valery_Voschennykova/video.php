<?php
/*
Template Name: Видео
*/

get_header(); ?>

			<div class="main clearfix">

			<div class="container">

				<div class="main__top main__top-inner main__top-about">
					<?php the_title( '<h1 class="main__title main__title-inner main__title-about">', '</h1>' ); ?>
										        <div style=" margin-top: 5%;">
<?php
 $paged = get_query_var( 'paged' ) ? absint( get_query_var( 'paged' ) ) : 1;
        $temp = $wp_query; $wp_query= null;
        $wp_query = new WP_Query(
                               array( 'category_name'  => 'видео',
                                       'posts_per_page' => 4,
	                                   'paged'          => $paged,
	                                   'post_type' => 'post'
                                   ));
$big = 999999999;
 paginate_links( array(
	'base' => str_replace( $big, '%#%', esc_url( get_pagenum_link( $big ) ) ),
	'format' => '?paged=%#%',
	'current' => max( 1, get_query_var('paged') ),
	'total' => $the_query->max_num_pages,
    'mid_size' => 2,
    'prev_text' => __( 'Back', 'textdomain' ),
    'next_text' => __( 'Onward', 'textdomain' ),


) );
                echo paginate_links();
?>
</div> 
				</div>

           
			
		
	
				<div class="media__block media__block-video maxheightset maxheightset_video" style="text-align: center;">
 <?php
        $paged = get_query_var( 'paged' ) ? absint( get_query_var( 'paged' ) ) : 1;
        $temp = $wp_query; $wp_query= null;
        $wp_query = new WP_Query(
                                array( 'category_name'  => 'видео',
                                       'posts_per_page' => 4,
	                                   'paged'          => $paged,
	                                   'post_type' => 'post'
                                   ));
           
        while ($wp_query->have_posts()) : $wp_query->the_post(); ?>
               		<div class="video__item">
						<a href="<?php echo get_the_excerpt(); ?>" class="popup"><img src="<?php echo the_post_thumbnail_url(array(440, 305)); ?>" alt=""></a>
					</div>
        			
			               
                         
        <?php endwhile; ?>
 
 
        <?php wp_reset_postdata(); ?>
        </div>    

			</div>

			</div>

		</div>
	
	
	
	
	

<?php get_sidebar(); ?>
<?php get_footer(); ?>



