<?php
/*
Template Name: Photo
*/

get_header(); ?>

			<div class="main clearfix">

			<div class="container">

				<div class="main__top main__top-inner main__top-about">
					<?php the_title( '<h1 class="main__title main__title-inner main__title-about">', '</h1>' ); ?></h1>
				</div>

           
			
		
	
<div class="media__block media__block-photo popup-gallery maxheightset">
					

					 <?php  $childrens = get_post_gallery_images(); 

    foreach ( $childrens as $children ) { ?>
       
    <div class="photo__item">
						<a href="<?php  echo $children; ?>" class="popupimg"><img src="<?php  echo $children; ?>" alt=""></a>
	</div>
<?php } ?> 
				</div>

			</div>

		</div>
	
	
	
	
	

<?php get_sidebar(); ?>
<?php get_footer(); ?>



