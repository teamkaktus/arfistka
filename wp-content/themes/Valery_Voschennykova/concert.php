<?php
/*
Template Name: Концерты
*/

get_header(); ?>

			<div class="main clearfix">

			<div class="container">

				<div class="main__top main__top-inner main__top-about">
					<?php the_title( '<h1 class="main__title main__title-inner main__title-about">', '</h1>' ); ?>
					        <div style="text-align: left; margin-top: 5%;">
<?php
 $paged = get_query_var( 'paged' ) ? absint( get_query_var( 'paged' ) ) : 1;
        $temp = $wp_query; $wp_query= null;
        $wp_query = new WP_Query(
                                array( 'category_name'  => 'концерты',
                                       'post_status' => 'future',
                                       'posts_per_page' => 2,
	                                   'paged'          => $paged,
	                                   'post_type' => 'post'
                                   ));
$big = 999999999;
 paginate_links( array(
	'base' => str_replace( $big, '%#%', esc_url( get_pagenum_link( $big ) ) ),
	'format' => '?paged=%#%',
	'current' => max( 1, get_query_var('paged') ),
	'total' => $the_query->max_num_pages,
    'mid_size' => 2,
    'prev_text' => __( 'Back', 'textdomain' ),
    'next_text' => __( 'Onward', 'textdomain' ),


) );
                echo paginate_links();
?>
</div> 
				</div>
          <div class="concert__block">
           
			                        <?php
       
           
        while ($wp_query->have_posts()) : $wp_query->the_post();
                $goldata = the_date('Y-m-d h:i', '', '', FALSE);
                $thedate = date('d.m.Y',strtotime($goldata));
                $thetime = date('h:i',strtotime($goldata));
             
                ?>
               
        			
					<div class="concert__item">
						<div class="concert__img">
							<img src="<?php echo the_post_thumbnail_url(array(250, 350)); ?>" alt="">
							<div class="item__overlay">
								<span class="date"><?php echo $thedate; ?></span>
								<span class="time"><?php echo $thetime; ?></span>
								<span class="address"><?php the_content(); ?></span>
							</div>
						</div>
						<h3 class="concert__title"><?php the_title(); ?></h3>
						<span class="concert__entry"><?php the_excerpt(); ?></span>
					</div>
			               
                         
        <?php endwhile; ?>
 
 
        <?php wp_reset_postdata(); ?>
        </div>

		
		
					<div class="concert__form">
					<?php if(get_bloginfo('language')=='ru-RU' ){ ?>
                     <span class="form__title">Вы можете подписаться на новости<br>о предстоящих концертах</span>
					<?php $widgetNL = new WYSIJA_NL_Widget(true);
                         echo $widgetNL->widget(array('form' => 1, 'form_type' => 'php')); ?>
                    <?php } ?>
                    <?php if(get_bloginfo('language')=='en-US' ){ ?>
                    <span class="form__title">You can subscribe to news<br>about upcoming concerts</span>
					<?php $widgetNL = new WYSIJA_NL_Widget(true);
                         echo $widgetNL->widget(array('form' => 2, 'form_type' => 'php')); ?>
                    <?php } ?>    
              

                        
                        
                        
                        
                        
		
				</div>

			</div>

		</div>
		<div class="bottom__overlay"></div>
	
	
	
	
	

<?php get_sidebar(); ?>
<?php get_footer(); ?>



