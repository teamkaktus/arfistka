<?php
/*
Template Name: мероприятие
*/

get_header(); ?>

		<div class="main main__party clearfix">

			<div class="container">

				<div class="main__top main__top-inner main__top-about main__top-party">
					<?php the_title( '<h1 class="main__title main__title-inner main__title-about">', '</h1>' ); ?></h1>
				</div>

           
			
		
	
				<div class="party__block clearfix maxheightset">
                    <?php the_post(); the_content(); ?>
			    </div>
			    
			    
			    <div class="media__block media__block-party maxheightset">
			       <?php 
            $query1 = new WP_Query('page_id=178');
            while($query1->have_posts()) $query1->the_post(); ;?>       
					<div class="media__title"><?php the_title(); ?></div>
<?php wp_reset_query(); ?>
			
			 <?php
        $temp = $wp_query; $wp_query= null;
        $wp_query = new WP_Query(
                                array( 'category_name'  => 'репертуар'
                                   ));
           
        while ($wp_query->have_posts()) : $wp_query->the_post(); ?>
                       <div class="media__category party_audio">
						<span class="category__title"><?php the_title(); ?></span>
                            <?php the_content(); ?>
					</div> 
			               
                         
        <?php endwhile; ?>
 
 
        <?php wp_reset_postdata(); ?>
        </div>    
    
          			 
			
			
			
				</div>
            
			 

			</div>

	
	
	
	
	

<?php get_sidebar(); ?>
<?php get_footer(); ?>



